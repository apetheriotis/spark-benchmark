/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.apetheriotis.sparkbenchmark

import _root_.kafka.serializer.StringDecoder
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._

object KafkaWordCount {
  def main(args: Array[String]) {

    if (args.length < 3) {
      System.err.println("Usage: KafkaWordCount <zookeepers> <topic> <numOfWorkers>")
      System.exit(1)
    }

    val Array(zookeepers, topic, numOfSlaves) = args

    // Setup kafka parameters
    val kafkaParams = Map[String, String](
      "zookeeper.connect" -> zookeepers,
      "group.id" -> "LogTucSpark",
      "zookeeper.connection.timeout.ms" -> "10000",
      "auto.commit.interval.ms" -> "10000",
      "auto.offset.reset" -> "largest")

    SparkLogging.setStreamingLogLevels()

    val sparkConf = new SparkConf().setAppName("KafkaWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    ssc.checkpoint("checkpoint")

    // --- Configure data stream --- //
    val commentsKafkaStream = (1 to numOfSlaves.toInt).map {
      _ =>
        KafkaUtils.createStream[String, String, StringDecoder,
          StringDecoder](ssc, kafkaParams, Map(topic -> 1),
            StorageLevel.MEMORY_ONLY_SER_2).map(_._2)
    }
    val stream = ssc.union(commentsKafkaStream).repartition(1)
    stream.print()

    // --- Filter only user id --- //
    val dbLogs = stream.map(comment => comment.split("_____")(5))

    println("Count:" + dbLogs.count())
//    println("Count:" + dbLogs.)

    var recordNo = 0
    dbLogs.foreachRDD { rdd =>
      recordNo =  recordNo +1
      rdd.foreach(record =>
        println(recordNo + " -> " + record)
      )
    }


    ssc.start()
    ssc.awaitTermination()
  }

}