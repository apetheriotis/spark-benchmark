/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.apetheriotis.sparkbenchmark


import com.twitter.algebird._
import kafka.serializer.StringDecoder
import org.apache.spark
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{StreamingContext, _}
import org.apache.spark.streaming.kafka._

import CMSHasherImplicits._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.math.Ordering.LongOrdering


/**
 * Consumes messages from one or more topics in Kafka and does wordcount.
 * Usage: DirectKafkaWordCount <brokers> <topics>
 * <brokers> is a list of one or more Kafka brokers
 * <topics> is a list of one or more kafka topics to consume from
 *
 * Example:
 * $ bin/run-example streaming.DirectKafkaWordCount broker1-host:port,broker2-host:port \
 * topic1,topic2
 */
object DirectKafkaWordCount extends spark.Logging {


  val DELTA = 1E-3
  val EPS = 0.01
  val SEED = 1
  val PERC = 0.001
  val HEAVY_HITTERS_PERCENTAGE = 0.001d
  val TOP_K = 10


  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println( s"""
                             |Usage: DirectKafkaWordCount <brokers> <topics>
                             | <brokers> is a list of one or more Kafka brokers
                             | <topics> is a list of one or more kafka topics to consume from
                             |
        """.stripMargin)
      System.exit(1)
    }

    SparkLogging.setStreamingLogLevels()

    val Array(brokers, topics) = args


    //    implicit  import  com.twitter.algebird.CMSHasher[Long]

    val cmsMonoids = TopPctCMS.monoid[Long](EPS, DELTA, SEED, HEAVY_HITTERS_PERCENTAGE)
    var globalCMS = cmsMonoids.zero


    // Create context with 2 second batch interval
    val sparkConf = new SparkConf()
      .setAppName("DirectKafkaWordCount")
      .setMaster("spark://10.240.147.47:7077")
      .setJars(Array(StreamingContext.jarOfClass(this.getClass).get))
    val ssc = new StreamingContext(sparkConf, Seconds(2))

//    // Create context
//    val ssc = new StreamingContext("spark://10.240.147.47:7077", "LogTUC-Streaming", Seconds(2),
//      "/opt/spark/spark-0.9.0-incubating/", StreamingContext.jarOfClass(this.getClass))
//


    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, String](
      "metadata.broker.list" -> brokers,
      "group.id" -> "direct"
//      "auto.offset.reset" -> "smallest"
    )


    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaParams, topicsSet)

    // Get the lines, split them into words, count the words and print
    val commentMetadata = messages.map(_._2)
    val commenterIds = commentMetadata.map(commentEntry => commentEntry.split("_____")(5).toLong)
    val wordCounts = commenterIds.map(x => (x, 1L)).reduceByKey(_ + _)


    val approxTopUsers = commenterIds.mapPartitions(ids => {
      ids.map(id => cmsMonoids.create(id))
    }).reduce(_ ++ _)


    wordCounts.print()

    approxTopUsers.foreachRDD(rdd => {
      if (rdd.count() != 0) {
        val partial = rdd.first()
        val partialTopK = partial.heavyHitters.map(id =>
          (id, partial.frequency(id).estimate)).toSeq.sortBy(_._2).reverse.slice(0, TOP_K)
        globalCMS ++= partial
        val globalTopK = globalCMS.heavyHitters.map(id =>
          (id, globalCMS.frequency(id).estimate)).toSeq.sortBy(_._2).reverse.slice(0, TOP_K)
        println("Approx heavy hitters at %2.2f%% threshold this batch: %s".format(PERC,
          partialTopK.mkString("[", ",", "]")))
        println("Approx heavy hitters at %2.2f%% threshold overall: %s".format(PERC,
          globalTopK.mkString("[", ",", "]")))
      }
    })

    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }
}